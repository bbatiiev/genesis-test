<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArtistAlbums
 *
 * @ORM\Table(name="artist_albums")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArtistAlbumsRepository")
 */
class ArtistAlbums
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="artist_id", type="integer")
     */
    private $artistId;

    /**
     * @var int
     *
     * @ORM\Column(name="album_id", type="integer")
     */
    private $albumId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set artistId
     *
     * @param integer $artistId
     *
     * @return ArtistAlbums
     */
    public function setArtistId($artistId)
    {
        $this->artistId = $artistId;

        return $this;
    }

    /**
     * Get artistId
     *
     * @return int
     */
    public function getArtistId()
    {
        return $this->artistId;
    }

    /**
     * Set albumId
     *
     * @param integer $albumId
     *
     * @return ArtistAlbums
     */
    public function setAlbumId($albumId)
    {
        $this->albumId = $albumId;

        return $this;
    }

    /**
     * Get albumId
     *
     * @return int
     */
    public function getAlbumId()
    {
        return $this->albumId;
    }
}

